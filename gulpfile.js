var gulp        = require('gulp');
var gutil       = require('gulp-util');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var sass        = require('gulp-sass');
var sourceMaps  = require('gulp-sourcemaps');
var imagemin    = require('gulp-imagemin');
var minifyCSS   = require('gulp-minify-css');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var gulpSequence = require('gulp-sequence').use(gulp);
var shell       = require('gulp-shell');
var plumber     = require('gulp-plumber');

var autoPrefixBrowserList = ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'];

gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: "app/"
        },
        options: {
            reloadDelay: 250
        },
        notify: false
    });
});


//compressing images & handle SVG files
gulp.task('images', function(tmp) {
    gulp.src(['app/images/*.jpg', 'app/images/*.png'])
        .pipe(plumber())
        .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
        .pipe(gulp.dest('app/images'));
});

//compressing images & handle SVG files
gulp.task('images-deploy', function() {
    gulp.src(['app/images/**/*', '!app/images/README'])
        .pipe(plumber())
        .pipe(gulp.dest('dist/images'));
});

//compiling Javascript
gulp.task('scripts', function() {
    return gulp.src([
                    'app/scripts/src/_includes/vendor/jquery-3.2.1.min.js',
                    'app/scripts/src/_includes/vendor/bootstrap.min.js',
                    'app/scripts/src/_includes/**/*.js',
                    'app/scripts/src/**/*.js'])
                .pipe(plumber())
                .pipe(concat('app.js'))
                .on('error', gutil.log)
                .pipe(gulp.dest('app/scripts'))
                .pipe(browserSync.reload({stream: true}));
});

//compiling Javascripts for deployment
gulp.task('scripts-deploy', function() {
    return gulp.src([
                    'app/scripts/src/_includes/vendor/jquery-3.2.1.min.js',
                    'app/scripts/src/_includes/vendor/bootstrap.min.js',
                    'app/scripts/src/_includes/**/*.js',
                    'app/scripts/src/**/*.js'])
                .pipe(plumber())
                .pipe(concat('app.js'))
                .pipe(uglify())
                .pipe(gulp.dest('dist/scripts'));
});

//compiling SCSS files
gulp.task('styles', function() {
    return gulp.src('app/styles/scss/init.scss')
                .pipe(plumber({
                  errorHandler: function (err) {
                    console.log(err);
                    this.emit('end');
                  }
                }))
                .pipe(sourceMaps.init())
                .pipe(sass({
                      errLogToConsole: true,
                      includePaths: [
                          'app/styles/scss/'
                      ]
                }))
                .pipe(autoprefixer({
                   browsers: autoPrefixBrowserList,
                   cascade:  true
                }))
                .on('error', gutil.log)
                .pipe(concat('styles.css'))
                .pipe(sourceMaps.write())
                .pipe(gulp.dest('app/styles'))
                .pipe(browserSync.reload({stream: true}));
});

//compiling SCSS files for deployment
gulp.task('styles-deploy', function() {
    return gulp.src('app/styles/scss/init.scss')
                .pipe(plumber())
                .pipe(sass({
                      includePaths: [
                          'app/styles/scss',
                      ]
                }))
                .pipe(autoprefixer({
                  browsers: autoPrefixBrowserList,
                  cascade:  true
                }))
                .pipe(concat('styles.css'))
                .pipe(minifyCSS())
                .pipe(gulp.dest('dist/styles'));
});

gulp.task('html', function() {
    return gulp.src('app/*.html')
        .pipe(plumber())
        .pipe(browserSync.reload({stream: true}))
        .on('error', gutil.log);
});

//migrating over all HTML files for deployment
gulp.task('html-deploy', function() {
    gulp.src('app/*')
        .pipe(plumber())
        .pipe(gulp.dest('dist'));

    gulp.src('app/.*')
        .pipe(plumber())
        .pipe(gulp.dest('dist'));

    gulp.src('app/fonts/**/*')
        .pipe(plumber())
        .pipe(gulp.dest('dist/fonts'));

    gulp.src(['app/styles/*.css', '!app/styles/styles.css'])
        .pipe(plumber())
        .pipe(gulp.dest('dist/styles'));
});

//cleans our dist directory in case things got deleted
gulp.task('clean', function() {
    return shell.task([
      'rm -rf dist'
    ]);
});

//create folders using shell
gulp.task('scaffold', function() {
  return shell.task([
      'mkdir dist',
      'mkdir dist/fonts',
      'mkdir dist/images',
      'mkdir dist/scripts',
      'mkdir dist/styles'
    ]
  );
});

gulp.task('default', ['browserSync', 'scripts', 'styles'], function() {
    //a list of watchers
    gulp.watch('app/scripts/src/**', ['scripts']);
    gulp.watch('app/styles/scss/**', ['styles']);
    gulp.watch('app/images/**', ['images']);
    gulp.watch('app/*.html', ['html']);
});

//build
gulp.task('build', gulpSequence('clean', 'scaffold', ['scripts-deploy', 'styles-deploy', 'images-deploy'], 'html-deploy'));
